﻿# Host: localhost  (Version: 5.5.27-log)
# Date: 2013-11-22 19:03:59
# Generator: MySQL-Front 5.3  (Build 1.0)

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='ALLOW_INVALID_DATES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;

USE `anx`;

#
# Source for table "category"
#

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(126) DEFAULT NULL,
  `title` varchar(126) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

#
# Data for table "category"
#

INSERT INTO `category` VALUES (1,'not-specified','Not specified'),(2,'animals','Animals'),(3,'aggie-ben','Aggie & Ben'),(4,'sports','Sports'),(5,'princesses','Princesses'),(6,'friends-family','Friends & Family'),(7,'bugs','Bugs'),(8,'vehicles','Vehicles'),(9,'music','Music'),(10,'debbie-friends','Debbie & Friends'),(11,'gustafer-yellowgold','Gustafer Yellowgold'),(12,'bed-thyme','Bed Thyme'),(13,'space-race','Space Race'),(14,'little-pim','Little Pim');

#
# Source for table "mode"
#

DROP TABLE IF EXISTS `mode`;
CREATE TABLE `mode` (
  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(126) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "mode"
#

INSERT INTO `mode` VALUES (1,'Read to me'),(2,'Read and play'),(3,'Read it myself'),(4,'Sing with me');

#
# Source for table "stat"
#

DROP TABLE IF EXISTS `stat`;
CREATE TABLE `stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(64) DEFAULT NULL COMMENT 'Event category.  Used to determine the type of event.',
  `action` varchar(64) DEFAULT NULL COMMENT 'Event action',
  `label` varchar(255) DEFAULT NULL COMMENT 'Event label.  Most commonly used to represent a story or category slug',
  `type` varchar(16) NOT NULL DEFAULT 'event',
  `value` int(11) unsigned DEFAULT NULL COMMENT 'Event Value.  Used to represent the duration (timer) value',
  `user_id` int(11) unsigned DEFAULT NULL,
  `user_name` varchar(64) DEFAULT NULL,
  `partner_id` tinyint(3) unsigned DEFAULT '0' COMMENT 'Which business account is this related to',
  `cd` varchar(255) DEFAULT NULL COMMENT 'Slash delimited string describing the path to the current screen',
  `cid` varchar(255) DEFAULT NULL COMMENT 'Client ID.  Anonymous user tracker.  One per install.',
  `mode_id` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'cd1. In GA, were using this as userType.  But in spkAnx, this would represent the story mode',
  `si` varchar(255) DEFAULT NULL COMMENT 'Story Instance ID.  Unique ID for use with Story start and duration events',
  `ip` int(10) unsigned NOT NULL DEFAULT '0',
  `viewed_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "stat"
#

INSERT INTO `stat` VALUES (1,NULL,'test',NULL,'event',NULL,NULL,NULL,0,NULL,NULL,1,NULL,2130706433,'2013-11-22 19:01:56'),(2,NULL,'test',NULL,'event',NULL,NULL,NULL,0,NULL,NULL,1,NULL,2130706433,'2013-11-22 19:02:28');

#
# Source for table "story"
#

DROP TABLE IF EXISTS `story`;
CREATE TABLE `story` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(126) DEFAULT NULL,
  `title` varchar(126) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

#
# Data for table "story"
#

INSERT INTO `story` VALUES (1,'a-christmas-carol','A Christmas Carol'),(2,'aggie-and-ben-the-surprise','Aggie and Ben: The Surprise'),(3,'aggie-and-ben-just-like-aggie','Aggie and Ben: Just Like Aggie'),(4,'aggie-and-ben-the-scary-thing','Aggie and Ben: The Scary Thing'),(5,'aggie-the-brave-a-visit-to-the-vet','Aggie the Brave: A Visit to the Vet'),(6,'aggie-the-brave-the-long-day','Aggie the Brave: The Long Day'),(7,'aggie-the-brave-get-well-soon','Aggie the Brave: Get Well Soon'),(8,'aladdin','Aladdin'),(9,'anansi-tries-to-steal-all-the-wisdom-of-the-world','Anansi Tries to Steal All the Wisdom of the World'),(10,'baa-baa-black-sheep','Baa, Baa, Black Sheep'),(11,'basil-the-royal-kitchen-mouse','Basil the Royal Kitchen Mouse'),(12,'beauty-and-the-beast','Beauty and the Beast'),(13,'bedtime-for-peter','Bedtime for Peter'),(14,'beehive','Beehive'),(15,'belindas-halloween','Belinda\'s Halloween'),(16,'billy-and-the-basketball-game','Billy and the Basketball Game'),(17,'billy-and-the-bratty-new-baby','Billy and the Bratty New Baby'),(18,'billy-and-the-brown-out','Billy and the Brownout'),(19,'billy-and-the-bully','Billy and the Bully'),(20,'el-hombre-el-nino-y-el-burro','El Hombre, el Niño, y el Burro'),(21,'billy-and-the-goodbye','Billy and the Goodbye'),(22,'billy-and-the-thankless-thanksgiving','Billy and the Thankless Thanksgiving'),(23,'cheddar','Cheddar'),(24,'chicken-little','Chicken Little'),(25,'cinderella','Cinderella'),(26,'creepy-the-beast','Creepy the Beast'),(27,'do-your-ears-hang-low','Do Your Ears Hang Low?'),(28,'el-lobo-con-piel-de-oveja','El Lobo con Piel de Oveja'),(29,'el-viente-norte-y-el-sol','El Viente Norte Y El Sol'),(30,'el-zorro-y-las-uvas','El Zorro y las Uvas'),(31,'five-little-monkeys','Five Little Monkeys'),(32,'frankenstein','Frankenstein'),(33,'frere-jacques','Frère Jacques'),(34,'golden-slumbers','Golden Slumbers'),(35,'goldilocks-and-the-3-bears','Goldilocks and the Three Bears'),(36,'gothel-the-strange','Gothel the Strange'),(37,'hansel-and-gretel','Hansel and Gretel'),(38,'i-saw-a-ship-a-sailing','I Saw a Ship-a-Sailing'),(39,'im-a-little-teapot','I\'m a Little Teapot'),(40,'im-from-the-sun','I\'m from the Sun'),(41,'its-raining-its-pouring','It\'s Raining, It\'s Pouring'),(42,'jack-and-jill','Jack and Jill'),(43,'jack-and-the-beanstalk','Jack and the Beanstalk'),(44,'jack-be-nimble','Jack Be Nimble'),(45,'la-bella-durmiente','La Bella Durmiente'),(46,'la-hormiga-y-el-saltamontes','La Hormiga y el Saltamontes'),(47,'la-liebre-y-la-tortuga','La Liebre y la Tortuga'),(48,'little-bo-peep','Little Bo Peep'),(49,'little-boy-blue','Little Boy Blue'),(50,'little-bunny-foo-foo','Little Bunny Foo Foo'),(51,'little-jack-horner','Little Jack Horner'),(52,'little-miss-muffet','Little Miss Muffet'),(53,'little-red-riding-hood','Little Red Riding Hood'),(54,'los-tres-cerditos','Los Tres Cerditos'),(55,'nickity-nick','Nickity Nick'),(56,'oh-susanna','Oh Susanna'),(57,'old-king-cole','Old King Cole'),(58,'old-macdonald','Old MacDonald'),(59,'old-mother-hubbard','Old Mother Hubbard'),(60,'opposite','Opposite!'),(61,'piggies-in-the-pumpkin-patch','Piggies in the Pumpkin Patch'),(62,'pop-goes-the-weasel','Pop Goes the Weasel'),(63,'pterodactyl-tuxedo','Pterodactyl Tuxedo'),(64,'put-me-in-coach','Put Me In, Coach!'),(65,'queen-topsteads-lament','Queen Topstead\'s Lament'),(66,'quite-easily-lost','Quite Easily Lost'),(67,'rapunzel','Rapunzel'),(68,'santa-and-baby','Santa and Baby'),(69,'santas-new-jet','Santa\'s New Jet'),(70,'six-little-ducks','Six Little Ducks'),(71,'slim-gets-in-em','Slim Gets In Em'),(72,'earth-eagle-wingstrong','Earth: Eagle Wingstrong'),(73,'mars-colonel-coot','Mars: Colonel Coot'),(74,'moon-pigeon-pirelli','Moon: Pigeon Pirelli'),(75,'space-stations-vulture-albatross','Space Stations: Vulture Albatross'),(76,'stone-soup','Stone Soup'),(77,'take-me-out-to-the-ballgame','Take Me Out to the Ballgame'),(78,'the-ant-and-the-dove','The Ant and the Dove'),(79,'the-ant-and-the-grasshopper','The Ant and the Grasshopper'),(80,'the-elves-and-the-shoemaker','The Elves and the Shoemaker'),(81,'the-emperors-new-clothes','The Emperor\'s New Clothes'),(82,'the-farmer-and-his-sons','The Farmer and his Sons'),(83,'the-farmer-in-the-dell','The Farmer in the Dell'),(84,'the-fisherman-and-his-wife','The Fisherman and his Wife'),(85,'the-fricassee-fairy','The Fricassee Fairy'),(86,'the-frog-prince','The Frog Prince'),(87,'the-gingerbread-man','The Gingerbread Man'),(88,'the-haunted-party','The Haunted Party'),(89,'the-jackals-strategy','The Jackal\'s Strategy'),(90,'the-little-half-chick','The Little Half-Chick'),(91,'the-little-mermaid','The Little Mermaid'),(92,'the-little-red-hen','The Little Red Hen'),(93,'the-man-who-wanted-nothing','The Man Who Wanted Nothing'),(94,'the-man-the-boy-and-the-donkey','The Man, the Boy and the Donkey'),(95,'the-miser-and-his-gold','The Miser and His Gold'),(96,'the-monkeys-go-fasting','The Monkeys Go Fasting'),(97,'the-mustard-slugs','The Mustard Slugs'),(98,'the-north-wind-and-the-sun','The North Wind and the Sun'),(99,'the-pied-piper-of-hamelin','The Pied Piper of Hamelin'),(100,'the-princess-and-the-pea','The Princess and the Pea'),(101,'the-rope-trick','The Rope Trick'),(102,'the-sages-daughter','The Sage\'s Daughter'),(103,'the-scholars-gift','The Scholar\'s Gift'),(104,'the-secret-recipe','The Secret Recipe'),(105,'the-selfish-giant','The Selfish Giant'),(106,'the-story-of-hanukkah','The Story of Hanukkah'),(107,'the-3-little-pigs','The Three Little Pigs'),(108,'the-town-mouse-and-the-country-mouse','The Town Mouse and the Country Mouse'),(109,'the-ugly-duckling','The Ugly Duckling'),(110,'the-wheels-on-the-bus','The Wheels on the Bus'),(111,'the-wise-little-girl','The Wise Little Girl'),(112,'the-wolf-in-sheeps-clothing','The Wolf in Sheep\'s Clothing'),(113,'this-little-piggy','This Little Piggy'),(114,'three-billy-goats-gruff','Three Billy Goats Gruff'),(115,'three-little-kittens','Three Little Kittens'),(116,'thumbelina','Thumbelina'),(117,'tom-thumb','Tom Thumb'),(118,'treasure-island','Treasure Island'),(119,'twinkle-twinkle-little-star','Twinkle, Twinkle, Little Star'),(120,'we-wish-you-a-merry-christmas','We Wish You A Merry Christmas'),(121,'why-anansi-has-eight-long-legs','Why Anansi Has Eight Long Legs'),(122,'wisconsin-poncho','Wisconsin Poncho'),(123,'wont-you-be-my-valentine','Won\'t You Be My Valentine'),(124,'you-are-my-sunshine','You Are My Sunshine'),(125,'el-avaro-y-su-oro','El Avaro y su Oro'),(126,'sleeping-beauty','Sleeping Beauty'),(127,'the-crows-gait','The Crow\'s Gait'),(128,'the-fox-and-the-grapes','The Fox and the Grapes'),(129,'hickory-dickory-dock','Hickory Dickory Dock'),(130,'the-boy-who-cried-wolf','The Boy Who Cried Wolf'),(131,'humpty-dumpty','Humpty Dumpty'),(132,'the-tortoise-and-the-hare','The Tortoise and the Hare'),(133,'when-i-play','When I Play'),(134,'one-two-buckle-my-shoe','One, Two, Buckle My Shoe'),(135,'sun-sabia-wingstrong','Sun: Sabia Wingstrong'),(136,'mercury-and-venus-robin-egger','Mercury and Venus: Robin Egger'),(137,'jupiter-master-crane','Jupiter: Master Crane'),(138,'home-run-ronnie','Home Run Ronnie!'),(139,'asteroid-belt-condor-wingstrong','Asteroid Belt: Condor Wingstrong'),(140,'head-shoulders-knees-and-toes','Head, Shoulders, Knees, & Toes'),(141,'bingo','Bingo'),(142,'the-valentine-contest','The Valentine Contest'),(143,'dinostory-stegosaurus','Dinostory: Stegosaurus'),(144,'dinostory-the-egg','Dinostory: The Egg'),(145,'dinostory-brachiosaurus','Dinostory: Brachiosaurus'),(146,'good-dog-aggie-a-bad-dog','Good Dog, Aggie: A Bad Dog'),(147,'good-dog-aggie-aggie-at-school','Good Dog, Aggie: Aggie at School'),(148,'good-dog-aggie-aggie-in-training','Good Dog, Aggie: Aggie in Training'),(149,'the-alphabet-song','The ABC Song'),(150,'the-eency-weency-spider','The Eency Weency Spider'),(151,'time-to-eat','Time to Eat'),(152,'find-it-for-the-party','Find It for the Party'),(153,'truck-stuck','Truck Stuck'),(154,'green-eleven','Green Eleven'),(155,'little-pim-colors-spanish','Little Pim: Colors - Spanish'),(156,'little-pim-colors-french','Little Pim: Colors - French'),(157,'pirate-or-parrot','Pirate or Parrot'),(158,'mechanimals','Mechanimals'),(159,'traffic-jamming','Traffic Jamming'),(160,'the-three-little-pigs','The Three Little Pigs'),(161,'little-pim-numbers-spanish','Little Pim: Numbers - Spanish'),(162,'little-pim-animals-spanish','Little Pim: Animals - Spanish'),(163,'little-pim-animals-french','Little Pim: Animals - French'),(164,'little-pim-feelings-french','Little Pim: Feelings - French'),(165,'little-pim-feelings-spanish','Little Pim: Feelings - Spanish'),(166,'little-pim-numbers-french','Little Pim: Numbers - French');

#
# Source for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;

#
# Data for table "user"
#

INSERT INTO `user` VALUES (111,'Max');

#
# Source for table "view_words"
#

DROP TABLE IF EXISTS `view_words`;
CREATE TABLE `view_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `story_id` smallint(3) unsigned DEFAULT NULL,
  `word_id` smallint(3) unsigned DEFAULT NULL,
  `view_id` int(11) unsigned DEFAULT NULL,
  `clicked_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Data for table "view_words"
#

INSERT INTO `view_words` VALUES (1,NULL,1,12,'2013-10-28 17:05:28'),(2,NULL,2,12,'2013-10-28 17:15:28'),(3,NULL,2,12,'2013-10-28 17:16:28'),(4,25,1,25,'2013-11-06 17:53:50'),(5,25,2,25,'2013-11-06 17:54:49'),(6,25,3,28,'2013-11-06 17:53:50'),(7,25,4,28,'2013-11-06 17:54:49');

#
# Source for table "word"
#

DROP TABLE IF EXISTS `word`;
CREATE TABLE `word` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `word` (`word`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Data for table "word"
#

INSERT INTO `word` VALUES (1,'hello'),(2,'world'),(3,'test'),(4,'hi');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
