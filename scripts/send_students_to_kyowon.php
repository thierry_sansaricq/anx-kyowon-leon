<?php
/**
* This script sending students data to kyowon
* 
* Shold be added to cron to run every n minutes if stats writing to database
* 
* Usage: php {filename.php} production|staging|development >> /path/to/logfile.txt
* Example: php /var/www/html/staging.anx.speakaboos.com/scripts/send_students_to_kyowon.php staging >> /var/www/html/staging.anx.speakaboos.com/data/logs/send_students_to_kyowon.log
* 
* @author     Max Chornopolsyi (max@speakaboos.com)
* @copyright  Copyright (c) 2014 Speakaboos, LLC. (http://www.speakaboos.com)
*/

if(!isset($argv[1])) {
    echo "Environment parameter missing\n";
    echo "Usage: php send_students_to_kyowon.php production|staging|development";
    die;
}

if($argv[1]) {
    putenv("APPLICATION_ENV={$argv[1]}");
}


// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
    
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
    
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

//only load resources we need for script, in this case db and mail
$application->getBootstrap()->bootstrap(array('db'));

//echo "Running env: ".APPLICATION_ENV."\n";


$config = new Zend_Config_Ini (APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);

global $config;

//Testing
//$test = array("kwUser7","kwUser8","kwUser9");
//$result = sendUsernamesToKyowon($test);
//echo "Response ".json_encode($result)."\n";


// Getting last processed id
$statLog = APPLICATION_PATH . '/../data/logs/send_students_to_kyowon_state.log';

$lastProcessedId = 0;

if(file_exists($statLog)) {
    $content = file_get_contents($statLog);
    $data = json_decode($content);
    if($data) {
        $lastProcessedId = (int) $data->last_processed_id;
    }
} else {
    saveLastProcessedId(0);
}


require_once 'Speakaboos/Stats/Model/Stat.php';

// Geting last stat id
$statModel = new Speakaboos_Stats_Model_Stat();

$lastStatId = $statModel->getLatestStudentViewId();


if(!$lastStatId) {
    ec("No data in stat table");
    die;
}

if(!$lastProcessedId) { // First ever script launch
    saveLastProcessedId($lastStatId);
    die;
}

if($lastStatId == $lastProcessedId) {
    //ec("No new data to process"); 
    die;
}

ec("Processing logs in range $lastProcessedId to $lastStatId\n");

$students = $statModel->getStudentNamesInRange($lastProcessedId, $lastStatId);

if($students) { 
    
    $response = sendUsernamesToKyowon($students);
    
    ec("Kyowon response: ".json_encode($response));
    
} else { // Empty data
    ec("No new studentnames found in logs");
}


saveLastProcessedId($lastStatId); // Save last processed logs state for next lunch
//echo "Last processed id updated to $lastStatId\n";
die;


function saveLastProcessedId($id) {
    global $statLog;
    
    $data = array('last_processed_id' => $id);
    $content = json_encode($data);
    return file_put_contents($statLog, $content);
    
}


/**
* Send kyowon usernames list to kyowon service
* 
* @param mixed $list
*/
function sendUsernamesToKyowon($students) {
    
    global $config;
    
    $apiUrl = $config->options->kyowon->url; // Kyowon API URL
    
    ec("Kyowon API URL: ".$apiUrl);
    
    $multyRequest = $config->options->kyowon->multy_requests;

    
    if(!$students) {
        return false;
    }
    
    // Multy requests at once
    if($multyRequest) {

        $mh = curl_multi_init();
        
        foreach($students as $i => $student) {
            
            $cu[$i] = curl_init();
            curl_setopt($cu[$i], CURLOPT_URL, $apiUrl);
            curl_setopt($cu[$i], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($cu[$i], CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($cu[$i], CURLOPT_POST, 1);
            curl_setopt($cu[$i], CURLOPT_POSTFIELDS, "un=$student");     
            
            curl_multi_add_handle($mh, $cu[$i]);                                
        }
            
        // execute the handles
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while($running > 0);
                  
        // get content and remove handles
        foreach($cu as $i => $c) {
            $response = curl_multi_getcontent($c);
                                    
            $results[$students[$i]] = getResponseCode($response); 
            
            curl_multi_remove_handle($mh, $c);
        }
         
        // all done
        curl_multi_close($mh);        
                
    } else { // Requests queue
        
        foreach($students as $i => $student) {
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "un=$student");       
            
            $response = curl_exec($ch);  
                        
            $results[$students[$i]] = getResponseCode($response);        
            
        }
        
    }
    
    return $results;
    
}


/**
* Get response code from kyoeon server reply
* 
* @param string $response
* 
* @return int
*/
function getResponseCode($response=false) {
    $code = false;
            
    if($response) {
        $data = json_decode($response);
        if($data && $data->retCd) {
            $code = (int) $data->retCd;
        }                
    }   
    
    return $code; 
}

// Echo with date, time and line break
function ec($str='') {
    
    echo dt() . " $str\n";
}

// Return formatted date
function dt() {
    return date("Y-m-d H:i:s");
}