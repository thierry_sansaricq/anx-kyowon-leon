<?php                                                                           
/**                                                                             
 * API Index Controller                                                             
 *                                                                              
 * Max Chernopolsky   max@speakboos.com
 * @copyright  Copyright (c) 2013 Speakaboos, LLC. (http://www.speakaboos.com)  
 * @version   
 */
class V1Controller extends Speakaboos_Api_Controller {

    /**
    * Redeclaration of default method param from 'method' to 'ea' for analytics API
    * 
    */
    //public function init() {
    //    $this->_methodParam = 'ea';
    //    return parent::init();
    //}
    
    public function indexAction() {
        
        // HealthChecker. Do nothing
        if(strpos($_SERVER['HTTP_USER_AGENT'], 'ELB-HealthChecker') !== false) {
            return true;
        }
        
        
        
        $action = $this->_method;
        
        if(method_exists($this, $action)) {
            return $this->$action(); 
        }      
        
        
        if($this->config->options->require_post) {
            $data = $this->getRequest()->getPost();
        } else {
            $data = $this->getRequest()->getParams();
        }        
        
        
     
        
        if(!$data['ea']) { // Reject incorrect recuests
            return false;
        }
        
        $queueTime       = $data['qt'];
        $localtimestamp  = time();
        $viewedTimestamp = $localtimestamp;
        
        
        
        if($queueTime > 0) { // queve parameter passed. Processing offline collected values
                        
            $secondsPassed = round($queueTime / 1000); // Converting queve milliseconds to seconds                                    
            $viewedTimestamp = $viewedTimestamp - $secondsPassed; // Setting view time in the past
        }        
        
        $data['ts'] = $viewedTimestamp;
        
        Speakaboos_Utils::log("Record time: {$data['ts']} ".date("Y-m-d H:i:s", $data['ts']));
        
        
        //var_dump($data['ts']); die;
        
        $data['ipaddr'] = $_SERVER['REMOTE_ADDR'];
        
        unset($data['controller'], $data['action'], $data['module'], $data['method'], $data['qt']);
        
        
        
        // Write to log
        if(!$this->config->options->write_to_database) {
                        
            $dataToString = json_encode($data);
            
            if($this->config->options->datalog) {
                $file = str_replace('{APPLICATION_PATH}', APPLICATION_PATH, $this->config->options->datalog);
                $logger = new Zend_Log();
                $logger->setTimestampFormat("d.m.Y H:i:s");
                $writer = new Zend_Log_Writer_Stream($file);
                $logger->addWriter($writer);
                $logger->log($dataToString, Zend_Log::DEBUG);                           
               
            } else {
                $this->error('No logfile specified');
            }
            
            
        } 
        else { // Write data directly to database
        
  
            
            
            Speakaboos_Utils::log($data, "Registering view");
            
            $statModel = new Speakaboos_Stats_Model_Stat();
            $recordId  = $statModel->registerAction($data);
            
            
            
            
            if(!$recordId) { // Record not added
            
                Speakaboos_Utils::log("Not added");
                return false;
            }
            
            Speakaboos_Utils::log("Record id: $recordId");
            
            
            // Event finished viewing story. Send message to kyowon
            if($this->config->options->kyowon->instant_notice && $data['ec'] == 'stories' && $data['ea'] == 'timer') { 
                
                            
                // Get student username
                $username = $statModel->getUsernameFromData($data);                
                
                
                
                
                // Sending notification to kyowon
                if($username) {
                    
                    $url = $this->config->options->kyowon->url;
                    
                    Speakaboos_Utils::log("Sending notice to $url Username: $username");
                    
                    $kyowon       = new Speakaboos_Stats_Kyowon();
                    $responseCode = $kyowon->send($username);
                    
                    // Log response
                    Speakaboos_Utils::log("Response code: $responseCode");
                    
                    /*
                    Speakaboos_Utils::log(
                        array(
                            'username' => $username,
                            'response' => $responseCode,
                            ), "Sending notice to kyowon");
                    */
                }

                
            }
        
            
        }        
        
    }
    
    
    
    // Sync data from speakaboos database
    public function syncAction() {
        
        
        $statModel = new Speakaboos_Stats_Model_Stat();
        $r = $statModel->syncDataSpeakaboos($this->config);

        $this->ok($r);
        
    }
 
    
    // Measure execution time
    public function __destruct() {
        
        //var_dump($this->_method); die;
        
        if(($this->_method == 'wvAction' || $this->_method == 'svAction') && $this->config->options->debug) {

            global $appStartTime;
            @list( $startMilli, $startSeconds, $endMilli, $endSeconds) = explode(' ',$appStartTime . ' ' . microtime());
            $generateTime = ($endSeconds+$endMilli)-($startSeconds+$startMilli);
        
            $timeDebug = sprintf('Executed in %.3fs', $generateTime);
              
            echo "DEBUG: ";
            if($this->config->options->write_to_database) {
                echo "Using database. ";
            } else {
                echo "Using logfile. ";
            }
              
              echo $timeDebug;
        }

    }      


}
