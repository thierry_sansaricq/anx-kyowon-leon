<?php
/**
* This script populates database with log records
* 
* Shold be added to cron to run daily
* 
* Usage: php {filename.php} production|staging|development >> /path/to/logfile.txt
* Example: php /var/www/html/staging.anx.speakaboos.com/scripts/process_logs.php staging >> /var/www/html/staging.anx.speakaboos.com/data/logs/process.log
* 
* @author     Max Chornopolsyi (max@speakaboos.com)
* @copyright  Copyright (c) 2013 Speakaboos, LLC. (http://www.speakaboos.com)
*/



$date = date("Y-m-d H:i:s");
echo "\n{$date}: Process stats logs\n";

if($argv[1]) {
    //echo "Set env: {$argv[1]}\n";
    putenv("APPLICATION_ENV={$argv[1]}");
}


// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
    
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
    
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

//only load resources we need for script, in this case db and mail
$application->getBootstrap()->bootstrap(array('db'));

echo "Running env: ".APPLICATION_ENV."\n";


$config = new Zend_Config_Ini (APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);

global $config;

$datalog    = str_replace('{APPLICATION_PATH}', APPLICATION_PATH, $config->options->datalog);
$processing = str_replace('{APPLICATION_PATH}', APPLICATION_PATH, $config->options->processing);
$processed  = str_replace('{APPLICATION_PATH}', APPLICATION_PATH, $config->options->processed);

if(!file_exists($datalog)) {
    echo "Logfile not found at $datalog\n";
    die;    
}

echo "Processing $datalog\n";

$r = rename($datalog, $processing);

if(!$r) {
    echo "Cannot rename logfile"; die;
}

require_once 'Speakaboos/Stats/Model/Stat.php';
require_once 'Speakaboos/Stats/Model/Word.php';
require_once 'Speakaboos/Stats/Model/StoryWords.php';
require_once 'Speakaboos/Stats/Model/User.php';

$statModel = new Speakaboos_Stats_Model_Stat();

$data = file($processing);

//$processedUserIds = array();
$processedKyowonUsers = array();

$cnt = 0;
foreach($data as $row) {
    $parts = explode('DEBUG (7): ', $row);
    $record = $parts[1];
    $request = (array) json_decode($record);
    //print_r($request);
    
    $id = $statModel->registerAction($request);
    if($id) {
        
        //$processedUserIds[] = (int) $request['u'];
        
        if($request['t'] == 'event' && $request['ec'] == 'stories' && $request['ea'] == 'timer' && $request['cd3']) {
            $processedKyowonUsers[] = $request['cd3'];
        }
        
        $cnt++;
    }
}

$processedFileName = $processed.date("Ymd_His").".log";

rename($processing, $processedFileName);


echo "Records added: $cnt\n";


// Preparing info for kyowon
if(!empty($processedKyowonUsers)) {


    $list = array_unique($processedKyowonUsers);

    if($list) {
        global $config;
        sendUsernamesToKyowon($list);
    }
}



/**
* Send kyowon usernames list to kyowon service
* 
* @param mixed $list
*/
function sendUsernamesToKyowon($list) {
    
    global $config;
    
    $apiUrl = $config->options->kyowon->url; // Kyowon API URL
    
    $students   = json_encode($list);
    
    $request = $apiUrl."?students=".$students;
    
    echo "Sending data to kyowon service: $request\n";
    
    //$r = file_get_contents($request);    
    //return $r;
    
}