<?php
/**
* This script populates stats database with data from application database.
* Data transfered: 
* 1) linear and interactive stories titles, thumbnails.
* 2) User ids, names, logins
* 
* Shold be added to cron to run daily
* 
* Usage: php {filename.php} production|staging|development >> /path/to/logfile.txt
* Example: php /var/www/html/staging.anx.speakaboos.com/scripts/sync_data.php staging >> /var/www/html/staging.anx.speakaboos.com/data/logs/sync.log
* 
* @author     Max Chornopolsyi (max@speakaboos.com)
* @copyright  Copyright (c) 2013 Speakaboos, LLC. (http://www.speakaboos.com)
*/


$date = date("Y-m-d H:i:s");
echo "\n{$date}: Copy data from app to stats database\n";

if($argv[1]) {
    //echo "Set env: {$argv[1]}\n";
    putenv("APPLICATION_ENV={$argv[1]}");
}


// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
    
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
    
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));



echo "Running env: ".APPLICATION_ENV."\n";

require_once 'Zend/Config/Ini.php';

$config = new Zend_Config_Ini (APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);

$appDb   = connect($config->options->appdb);
$statsDb = connect($config->resources->db->params);

// Sync stories
$sql = "
    select 
    c.id, 
    c.slug, 
    c.title,
    m.uri as thumbnail

    from content c
    left join content_media m on (m.content_id = c.id)
    where c.content_type_id = 1
    and m.media_type_id = 1
";


$r = mysql_query($sql, $appDb);

$c = 0;
if($r) {
    
    mysql_query('truncate story');

    while($row = mysql_fetch_assoc($r)) {
        
        /*
        $s = "INSERT IGNORE INTO `story` (`slug`, `title`, `thumbnail`) VALUES (
            '". mysql_escape_string($row['slug']) ."', 
            '". mysql_escape_string($row['title']) ."', 
            '{$row['thumbnail']}'
            )";
            */
            
        //$s = "INSERT IGNORE INTO `story` (`slug`, `title`) VALUES (
        $s = "INSERT INTO `story` (`slug`, `title`) VALUES (
            '". mysql_escape_string($row['slug']) ."', 
            '". mysql_escape_string($row['title']) ."'
            )";   
            
        //$dbg .= $s.";\n";
        
        //echo "{$row['slug']}\n";
        
        
        $i = mysql_query($s);
        if($i) {
            $c++;
        } else {
            echo $s."\n";
            echo mysql_error();
            echo "\n";
        }
    }    
} else {
    echo mysql_error();
}
echo "Stories added: $c\n";



// Sync categories


$sql = "
    SELECT 
    c.id, 
    c.slug, 
    c.title,
    m.uri as thumbnail

    FROM content c
    LEFT JOIN content_media m on (m.content_id = c.id)
    WHERE c.content_type_id IN (
        SELECT id FROM content_type WHERE parent = (SELECT id FROM content_type WHERE slug = 'category') 
    )
    AND m.media_type_id = 1
";


$r = mysql_query($sql, $appDb);

$c = 0;
if($r) {
    
    mysql_query('truncate category');
    
    while($row = mysql_fetch_assoc($r)) {
        

        /*
        $s = "INSERT IGNORE INTO `category` (`slug`, `title`, `thumbnail`) VALUES (
            '". mysql_escape_string($row['slug']) ."', 
            '". mysql_escape_string($row['title']) ."', 
            '{$row['thumbnail']}'
            )";
        */
            
        //$s = "INSERT IGNORE INTO `category` (`slug`, `title`) VALUES (
        $s = "INSERT INTO `category` (`slug`, `title`) VALUES (
            '". mysql_escape_string($row['slug']) ."', 
            '". mysql_escape_string($row['title']) ."'
            )";            
        
        
        $i = mysql_query($s);
        if($i) {
            $c++;
        } else {
            echo $s."\n";
            echo mysql_error();
            echo "\n";
        }
    }    
} else {
    echo mysql_error();
}
echo "Categories added: $c\n";


//echo "$dbg";


die;

// Adding users
$userSql = "
    select u.id, u.username,
    concat(p.first_name, ' ', p.last_name) as name
    from user u
    left join user_profile p on (p.user_id = u.id)
";


$r = mysql_query($userSql, $appDb);

$c = 0;
if($r) {
    mysql_query("delete from `stat_user`", $statsDb);
    while($row = mysql_fetch_assoc($r)) {

        $s = "insert into `stat_user` values ({$row['id']}, '{$row['username']}', '" . mysql_escape_string($row['name']) . "')";
        $i = mysql_query($s);
        if($i) {
            $c++;
        } else {
            echo "$s\n";
            echo mysql_error();
            echo "\n";
        }
    }    
} else {
    echo mysql_error();
}
echo "Users added: $c\n";






function connect($db) {
    
    $id = mysql_connect($db->host, $db->username, $db->password, true) or die ("Cannot connect to $db->host");
    mysql_select_db($db->dbname, $id);
    
    return $id;
}